script:

	@title = "TestEntityService.java.dtmp"
	@description = "JUnit test for entity service class"

	@outfile = $CLV_src_main_test_dir + "/service/Test" + $CLV_EntityName + "Service.java"

text:
package @{$CLV_base_package}.service;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import @{$CLV_base_package}.domain.@{$CLV_EntityName};
import @{$CLV_base_package}.domain.@{$CLV_EntityName}_;
import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;

/**
 * JUnit test service class for entity class: @{$CLV_EntityName}
 * @{$CLV_comment_text}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class Test@{$CLV_EntityName}Service {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private @{$CLV_EntityName}Service service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(@{$CLV_EntityName}Service service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			index++;
		}

		List<@{$CLV_EntityName}> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			index++;
		}

		List<@{$CLV_EntityName}> list = service.getAllEntities(@{$CLV_EntityName}_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (@{$CLV_EntityName} @{$CLV_lEntityName} : list) {
			assertTrue(lastId < @{$CLV_lEntityName}.getId());
			lastId = @{$CLV_lEntityName}.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			// index++;
		}

		List<@{$CLV_EntityName}> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		// long index = service.count();
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			// index++;
		}

		List<@{$CLV_EntityName}> list = service.getAllEntities(10, 10, @{$CLV_EntityName}_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (@{$CLV_EntityName} @{$CLV_lEntityName} : list) {
			assertTrue(lastId < @{$CLV_lEntityName}.getId());
			lastId = @{$CLV_lEntityName}.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			index++;
		}

		// all - FALSE
		List<@{$CLV_EntityName}> list = service.getAllEntities(false, 10, 10, @{$CLV_EntityName}_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (@{$CLV_EntityName} @{$CLV_lEntityName} : list) {
			assertTrue(lastId < @{$CLV_lEntityName}.getId());
			lastId = @{$CLV_lEntityName}.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 10, 10, @{$CLV_EntityName}_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (@{$CLV_EntityName} @{$CLV_lEntityName} : list) {
			assertTrue(lastId < @{$CLV_lEntityName}.getId());
			lastId = @{$CLV_lEntityName}.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {

		@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
		@{$CLV_lEntityName}.setKeyName("key " + 999);
		@{$CLV_lEntityName} = service.persist(@{$CLV_lEntityName});

		long id = @{$CLV_lEntityName}.getId();

		@{$CLV_EntityName} @{$CLV_lEntityName}2 = service.find(id);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
		@{$CLV_lEntityName}.setKeyName("key " + 999);
		@{$CLV_lEntityName} = service.persist(@{$CLV_lEntityName});

		long id = @{$CLV_lEntityName}.getId();

		@{$CLV_EntityName} @{$CLV_lEntityName}2 = service.find(id);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
		@{$CLV_lEntityName}.setKeyName("key " + 999);
		@{$CLV_lEntityName} = service.persist(@{$CLV_lEntityName});

		long id = @{$CLV_lEntityName}.getId();

		@{$CLV_EntityName} @{$CLV_lEntityName}2 = service.find(id);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());

		@{$CLV_lEntityName}2.setKeyName("key 65535");
		@{$CLV_lEntityName}2 = service.refresh(@{$CLV_lEntityName}2);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
		@{$CLV_lEntityName}.setKeyName("key " + 999);
		@{$CLV_lEntityName} = service.persist(@{$CLV_lEntityName});

		long id = @{$CLV_lEntityName}.getId();

		@{$CLV_EntityName} @{$CLV_lEntityName}2 = service.find(id);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());

		@{$CLV_lEntityName}2.setKeyName("key 65535");
		@{$CLV_lEntityName}2 = service.merge(@{$CLV_lEntityName}2);

		@{$CLV_lEntityName} = service.refresh(@{$CLV_lEntityName});

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
		@{$CLV_lEntityName}.setKeyName("key " + 999);
		@{$CLV_lEntityName} = service.persist(@{$CLV_lEntityName});

		long id = @{$CLV_lEntityName}.getId();

		@{$CLV_EntityName} @{$CLV_lEntityName}2 = service.find(id);

		assertEquals(@{$CLV_lEntityName}, @{$CLV_lEntityName}2);
		assertTrue(@{$CLV_lEntityName}.getId() == @{$CLV_lEntityName}2.getId());
		assertEquals(@{$CLV_lEntityName}.getKeyName(), @{$CLV_lEntityName}2.getKeyName());

		service.remove(@{$CLV_lEntityName});

		@{$CLV_EntityName} @{$CLV_lEntityName}3 = service.find(id);
		assertTrue(@{$CLV_lEntityName}3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {

		long index = service.count();
		for (int i = 200; i < 210; i++) {
			@{$CLV_EntityName} @{$CLV_lEntityName} = new @{$CLV_EntityName}();
			@{$CLV_lEntityName}.setKeyName("key " + i);
			service.persist(@{$CLV_lEntityName});
			index++;
		}

		List<@{$CLV_EntityName}> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
