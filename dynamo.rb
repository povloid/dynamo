#!/usr/bin/ruby

# The dymamical code generator from templites.
# author name -> Kopychenko Pavel
# CALL UN7TAD QTH  KAZAKHSTAN->TARAZ 
# was created on 2011 yar.
# license GPL-3  - 73!

require "getoptlong"
require "fileutils"

puts "Dynamo running"

SCRIPT_MARKER="script:"
TEXT_MARKER="text:"

# First initialising
#$clvars = Array.new

# The parse command line
opts = GetoptLong.new(
[ '--help', '-h', GetoptLong::NO_ARGUMENT ],
[ '--vars', '-v', GetoptLong::OPTIONAL_ARGUMENT ]
)

opts.each do |opt, arg|
  case opt
  when '--help'
    puts <<-EOF
    hello [OPTION] ... templite

    -h, --help:
       show help

    -vs, --vars:
       variables

    templite: The templite file.
          EOF
    exit
  when '--vars'
    vars = arg.split(",")
    
    $varsstr = vars*","
    
    vars.reverse_each do |e|
      kv = e.split("=",2)
      eval "$CLV_#{kv[0]}='#{kv[1]}'"
      #$clvars.push(CLVar.new(kv[0],kv[1]))
    end
  end
end

if ARGV.length == 0
  puts "Missing templite argument (try --help)"
  exit 0
end

puts "The vars: #{$varsstr}"

###
$templites = ARGV
argstr = ARGV*","
puts "The input templites size: #{ARGV.size}"
puts "The input templites: #{argstr}"

# Класс шаблона -----------------------------------------------------
class Templite

  attr_accessor :title, :description ,:file ,:outfile , :script, :text, :outtext
  
  def initialize(file)
    @file = file

    openFile
    
    makeScript
    
    makeText
    
    writeToFile
  end

  # Открыть фаил
  def openFile
    File.open(@file, "r") do |file|
      marker = nil
      @script = ""
      @text = ""
      file.each do |line|
        if line.strip == SCRIPT_MARKER
        marker = 0
        elsif line.strip == TEXT_MARKER
        marker = 1
        else
        @script << line if marker == 0
        @text << line if marker == 1
        end
      end
    end
  end

  # Обработать текст
  def makeText
    @outtext = @text
    
    global_variables.reverse_each do |e|
       if e =~ /^\$CLV_/
         t = nil
        eval "t = " << e 
        ###puts "e=#{e}=#{t}"
        t = "" if t == nil
        @outtext.gsub!("@{#{e}}", t) 
      end 
    end
    
    instance_variables.reverse_each do |e|
      @outtext.gsub!("@{#{e}}", instance_variable_get(e)) 
    end
    
    
    puts "-----------------------------------------------------"
    puts @outtext
    
    
  end

  # Выполнить скрипт
  def makeScript
    puts "* TEMPLITE #{@file} MAKE SCRIPT"
    eval @script
  end
  
  # Сохранить в фаил
  def writeToFile
    return if @outfile == nil
    
    puts "Write outtext to file: #{@outfile}"
    
    FileUtils.makedirs(File.dirname(@outfile))
    File.open(@outfile, "w") do |outfile|
      outfile.puts @outtext
    end
  end

end # ----------------------------------------------------------------, 


#Обработка всех шаблонов
$templites.reverse_each do |e|
  t = Templite.new(e)
end



